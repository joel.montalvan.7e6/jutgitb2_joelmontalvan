import java.io.File
import java.util.Scanner

const val sinColor = "\u001b[0m"
const val negro = "\u001b[30m"
const val rojo = "\u001b[31m"
const val magenta = "\u001b[35m"
const val blanco = "\u001b[37m"
const val cyan = "\u001b[36m"
const val verde = "\u001b[32m"


class Problem(
    private val tipusDeProblema:String,
    private val titolDelProblema:String,
    private val introDelProblema:String,
    private val input:String,
    private val output:String,
    private val inputPub:String,
    private val outputPub:String,
    private val inputPriv:String,
    private val outputPriv:String,
) {

    private var attempts = 0
    private var resolvedProblem = false

    fun questionProblem(){
        println(rojo + "Tipus de problema: ${this.tipusDeProblema}"+ "\n")
        println(rojo + "Problema: ${this.titolDelProblema}"+ "\n")
        println(rojo + this.introDelProblema+ "\n" )
        println(this.input)
        println(this.output+ "\n" + sinColor)
        println(blanco + "Entrada Exemple: ${this.inputPub}")
        println(blanco +"Sortida Exemple: ${this.outputPub}"+ "\n" + sinColor)
        println(rojo+ "Entrada del programa a fer: ${this.inputPriv}"+ "\n"+sinColor)
    }
    fun attemptsStatus(){
        println(cyan + "Intents en total: ${this.attempts}")
        print(cyan + "Problema:")
        if(this.resolvedProblem != false){
            print("No Resolt"+ "\n" + sinColor)
        }
        else println("Resolt" + sinColor)
    }

    fun answerProblemResolved(){
        while (true){
            this.attempts++
            println(verde + "Quin es el resultat final?")
            val scanner = Scanner(System.`in`)
            val answer = scanner.next()
            if(answer == this.outputPriv) {
                println(verde + "El JutgITB ho aprova \uD83D\uDC4D" + sinColor)
                this.attemptsStatus()
                this.resolvedProblem = true
                break
            }
            else println(verde + "El JutgITB no ho aprova, continua intentant-ho \uD83D\uDC4E"+sinColor)
            this.attemptsStatus()
        }
    }
}
fun main(){
    println(verde + """
       
░░░░░██╗██╗░░░██╗████████╗░██████╗░  ██╗████████╗██████╗░
░░░░░██║██║░░░██║╚══██╔══╝██╔════╝░  ██║╚══██╔══╝██╔══██╗
░░░░░██║██║░░░██║░░░██║░░░██║░░██╗░  ██║░░░██║░░░██████╦╝
██╗░░██║██║░░░██║░░░██║░░░██║░░╚██╗  ██║░░░██║░░░██╔══██╗
╚█████╔╝╚██████╔╝░░░██║░░░╚██████╔╝  ██║░░░██║░░░██████╦╝
░╚════╝░░╚═════╝░░░░╚═╝░░░░╚═════╝░  ╚═╝░░░╚═╝░░░╚═════╝░
    """.trimIndent() + "\n")

    println(verde + "Ets professor [P] o estudiant [E]?")

    while (true){
        val scanner = Scanner(System.`in`)
        val selectorIdentitat = scanner.next().uppercase()
        if (selectorIdentitat == "P"){
           identificadorProfessor()
            break
        }
        else if (selectorIdentitat == "E"){
            menuEstudiant()
            break
        }
        else println("No reconec aquesta classe de identitat" + "\n" + "Torna-ho a provar, siusplau")
    }


}

fun menuEstudiant(){
    println(verde + "Benvingut al JutgITB, preparat per resoldre tots el problemes proposats per mi?" + "\n")
    println(cyan + """
        1. SEGUIR AMB L'ITINERARI D'APRENENTATGE
        2. LLISTA DE PROBLEMAS
        3. CONSULTAR HISÒRIC DE PROBLEMES RESOLTS
        4. AJUDA
        5. EXIT
    """.trimIndent())
    while (true){
        val scanner= Scanner(System.`in`)
        val opcioTriada = scanner.nextInt()
        if (opcioTriada == 1){
            iniciDelPrograma()
            break
        }
        else if (opcioTriada == 2){
            println(magenta+ """
                1. TIPUS DE DADES
                2. CONDICIONALS
                3. BUCLES
                4. LLISTES
                5. MENU
            """.trimIndent()+sinColor)
            println(verde+"Quin problema t'agradaria resoldre?"+sinColor)
            while (true){
                val tipusProblemaEscollit = scanner.nextInt()
                when(tipusProblemaEscollit){
                    1 ->{

                        break
                    }
                    2 ->{
                        break
                    }
                    3 ->{
                        break
                    }
                    4 ->{
                        break
                    }
                    5 ->{
                        menuEstudiant()
                        break
                    }
                    else -> println(verde + "Aquesta opció no esta disponible, torna a intentar-ho"+sinColor)
                }
            }
        }
        else if (opcioTriada == 3){
            println(verde+"Carregant problemes resolts...")
            Thread.sleep(1000L)
            println(verde+"Problemes resolts en total:"+sinColor)
            break
        }
        else if (opcioTriada == 4){
            println(verde + """
                Benvingut al JutgITB,
               
                Soc una IA dessarollada per un programador informàtic del centre per tal de facilitar la vida tan a estudiants com al professorat.
                
                Basicament et propossare una quantitat de problemes informàtics per tal de resoldrel's per la teva compta, 
                aquí només hauras de introduir el resultat final per comprovar si el exercici ha sigut resolt o no.
                
                Tinc una llista de problemes de diferents seccions per tal de que puguis practicar les diverses implementacions que té la programació actualment.
                
                Al menu hi veuras les diferents ocpcions que pots triar:              
                
                - L’opció de seguir amb l’itinerari d’aprenentatge mira quins dels problemes han estat resolts per l’usuari i mostra el següent de la llista.
                
                - L’opció de llista de problemes mostra una llista amb tots els problemes que existeixen al sistema i en permet seleccionar-ne un.
                
                - L’històric de problemes resolts mostra els intents realitzats a cada problema i si aquest ha estat superat o no.
                
                Preparat per resoldre tots el problemes proposats per mi?
                Doncs bona sort futur programador.
            """.trimIndent())

            menuEstudiant()
            break
        }
        else if (opcioTriada == 5){
            println(verde + "Fins aviat!!!" + "\n" + "Espero que ho hagis disfrutat o... patit")
            break
        }
        else println(cyan + "Aquesta opció no esta disponible:" + sinColor)
    }
}


fun iniciDelPrograma(){
    var resolvedProblem = 0

    for(i in DifferentsProblems.indices){
        val problem = DifferentsProblems[i]
        problem.questionProblem()
        println(verde + "Vols resoldre aquest problema?")
        println(verde + "INTRODUEIX (SI) O (NO):" + sinColor)

        while (true){
            val scanner = Scanner(System.`in`)
            val validateAnswer = scanner.next().uppercase()
            if(validateAnswer == "SI"){
                resolvedProblem++
                problem.answerProblemResolved()
                println(cyan + "Problemes fets per el moment: $resolvedProblem/5"+ "\n")
                break
            }
            else if(validateAnswer == "NO"){
                println(verde + "No passa res, ja ho faràs en un altre moment")
                println(verde + "Passem al següent problema" + sinColor)
                println(cyan + "Problemes fets per el moment: $resolvedProblem/5"+ "\n")
                break
            }
            else println(cyan + "El JutgITB no ha no ha entès la teva resposta, torna a provar"+ "\n")
        }
    }
}

fun identificadorProfessor(){
    var attempts = 3
    println("No et creguis tan llest estimat estudiant, primer vull comprovar si de veritat ets un professor" + "\n" + "Introdueix contrasenya (3 INTENTS):")
    while (attempts > 0){
        val scanner = Scanner(System.`in`)
        val contrasenya = scanner.nextInt()

        if (contrasenya == 247789){
            modeProfessor()
            break
        }
        else{
            println(rojo + "Contrasenya incorrecta")
            attempts--
            if (attempts == 0){
                println("\n"+ verde+ "Només puc deduir dues coses, ets un estudiant o tens problemas per recordar les coses" + "\n")
                main()
                break
            }
        }
    }
}

fun modeProfessor(){
    println(verde + "Li dono la benvinguda al JutgITB estitmat professor")
    println("""
        1. AFEGIR NOUS PROBLEMAS
        2. FEINA DE L'ALUMNE
        3. EXIT
    """.trimIndent())

    while (true){
        val scanner = Scanner (System.`in`)
        val opcioTriada = scanner.nextInt()

        if (opcioTriada == 1){
            break
        }
        else if (opcioTriada == 2){
            break
        }
        else if (opcioTriada == 3){
            println("Fins aviat!!!" + "\n" + "Espero que hagis disfrutat fent patir als alumnes")
            break
        }
        else println("No esta disponible aquesta opció, torna a provar-ho")
    }
}


