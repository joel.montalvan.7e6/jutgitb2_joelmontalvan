val DifferentsProblems = listOf(
    Problem(
        "TIPUS DE DADES",
        "Pupitres",
        "En una escola tenim tres classes i volem saber quin és el nombre de taules que necessitarem tenir en total. " + "\n" +
                "Dependrà del nombre d'alumnes per aula. Cal tenir en compte que a cada taula hi caben 2 alumnes.",
        "Per l’entrada rebreu 3 enters que representen el nombre d’alumnes que hi ha a cada classe.",
        "La sortida ha de ser un enter que representi el nombre de pupitres necessaris.",
        "22 25 26",
        "37",
        "31 29 19",
        "41"
    ),
    Problem(
        "TIPUS DE DADES",
        "Calcula el descompte",
        "Llegeix el preu original i el preu actual i imprimeix el descompte (en %).",
        "Per l’entrada rebreu dos nombres decimals on, el primer, representa el preu original i, el segon, el preu amb descompte.",
        "Per la sortida sempre haureu d’imprimir un altre decimal amb el valor del descompte representat en %.",
        "1000 800",
        "20",
        "540 420",
        "22.22"
    ),
    Problem(
        "TIPUS DE DADES",
        "Quina és la mida de la meva pizza?",
        "Llegeix el diàmetre d'una pizza rodona i imprimeix la seva superfície. Pots usar Math.PI per escriure el valor de Pi.",
        "Per l’entrada rebreu un nombre decimal que representa el diàmetre d’una pizza.",
        "Per la sortida sempre haureu d’imprimir un altre decimal amb la superfície de la pizza.",
        "38",
        "1133.54",
        "55",
        "2375.83"
    ),
    Problem(
        "TIPUS DE DADES",
        "De Celsius a Fahrenheit",
        "Feu un programa que rebi una temperatura en graus Celsius i la converteixi en graus Fahrenheit",
        "Per l’entrada rebreu un nombre decimal que representa una temperatura en graus Celsius.",
        "Per la sortida sempre haureu d’imprimir un altre decimal amb el valor de la temperatura en graus Fahrenheit",
        "33.1",
        "91.58",
        "-3.2",
        "26.2"
    ),
    Problem(
        "TIPUS DE DADES",
        "Divisor de compte",
        "Fes un programa on, introduit el número de començals i el preu d'un sopar (que pot contenir cèntims), imprimeixi quan haurà de pagar cada començal.",
        "Per l’entrada rebreu un nombre enter amb el número de començals i un nombre decimal amb el cost del sopar.",
        "Per la sortida haureu d’imprimir quin és el cost del sopar per començal en Euros.",
        "4 76.56",
        "19.14€",
        "8 243.90",
        "30.4875€"
    ),
    Problem(
        "CONDICIONALS",
        "Salari",
        "A un treballador li paguen per hores, a partir de 40 hores de treball la tarifa per hora s'incrementa en un 50%. Escriu un\n" +
                "algorisme que demani les hores treballades i mostri com a resultat el salari del treballador, tenint en compte que la tarifa per\n" +
                "hora és de 40 €/h.",
        "Per l’entrada rebreu el nombre d’hores que ha treballat en una setmana, en format enter.",
        "La sortida ha de ser un enter que representi el salari que ha de rebre el treballador per aquella setmana de feina.",
        "40",
        "1600€",
        "55",
        "2500€"
    ),
    Problem(
        "CONDICIONALS",
        "És un bitllet vàlid",
        "L'usuari escriu un enter i s'imprimeix true si existeix un bitllet d'euros amb la quantitat entrada, false en qualsevol altre cas.",
        "Per l’entrada rebreu un nombre enter.",
        "Per la sortida sempre haureu d’imprimir un boolean a true si l’enter representa el valor d’un bitllet real i un boolean a false en\n" +
                "qualsevol altre cas.",
        "49",
        "false",
        "50",
        "true"
    ),
    Problem(
        "CONDICIONALS",
        "En rang",
        "Escriu un programa que llegeixi 5 enters. El primer i el segon creen un rang, el tercer i el quart creen un altre rang. Mostra true\n" +
                "si el cinquè valor està entre els dos rangs, si no false.",
        "Per l’entrada rebreu cinc nombres enters.",
        "Per la sortida sempre haureu d’imprimir true si el cinquè enter està dins del rang que formen el primer/segon i tercer/quart enter\n" +
                "o false en el cas contrari.",
        "10 40 20 30 25",
        "true",
        "10 40 20 30 55",
        "false"
    ),
    Problem(
        "CONDICIONALS",
        "Calcula la lletra del dni",
        "Fes un programa que, donat un número de dni, et calculi la lletra que li correspon.",
        "Per l’entrada rebreu un nombre enter, que representa un número de dni, és a dir, un nombre de 8 xifres exactes.",
        "Per la sortida haureu d’imprimir el número del dni amb la seva lletra corresponent.",
        "65004204",
        "65004204V",
        "65004203",
        "65004203Q"
    ),
    Problem(
        "CONDICIONALS",
        "Puges o baixes?",
        "Fes un programa que, donat tres nombres enters, indiqui si la seqüència és ascendent, descendent o cap de les dues.",
        "Per l’entrada rebreu tres nombres enters.",
        "Per la sortida haureu d’imprimir si la seqüència és ascendent, descendent o cap de les dues.",
        "1 2 3",
        "Ascendent",
        "9999 1 3245",
        "Cap de les dues"
    ),
    Problem(
        "BUCLES",
        "Calcula la suma dels N primers",
        "Escriu un programa que donat un nombre enter s’imprimeixi la suma de tots els enters fins a arribar a aquest.",
        "Per l’entrada rebreu un nombre enter.",
        "Heu d’imprimir un nombre enter que representi la suma de tots els nombres fins a arribar al d’entrada (inclòs).",
        "3",
        "6",
        "5",
        "15"
    ),
    Problem(
        "BUCLES",
        "Taula de multiplicar",
        "Escriu un programa que donat un nombre enter positiu, imprimeixi la seva taula de multiplicar.",
        "Per l’entrada rebreu un nombre enter positiu.",
        "La sortida ha d’imprimir la taula de multiplicar d’aquest nombre enter.",
        "3",
        "3x1 = 3\n" +
                "3x2 = 6\n" +
                "3x3 = 9\n" +
                "3x4 = 12\n" +
                "3x5 = 15\n" +
                "3x6 = 18\n" +
                "3x7 = 21\n" +
                "3x8 = 24\n" +
                "3x9 = 27\n" +
                "3x10 = 30",
        "24",
        "24x1 = 24\n" +
                "24x2 = 48\n" +
                "24x3 = 72\n" +
                "24x4 = 96\n" +
                "24x5 = 120\n" +
                "24x6 = 144\n" +
                "24x7 = 168\n" +
                "24x8 = 192\n" +
                "24x9 = 216\n" +
                "24x10 = 240"
    ),
    Problem(
        "BUCLES",
        "Nombre de dígits",
        "Escriu un programa que donat un nombre enter, escrigui el nombre de dígits que té.",
        "Per l’entrada rebreu un nombre enter.",
        "La sortida ha de ser “El número n té x dígits”.",
        "123456",
        "El número 123456 té 6 dígits.",
        "0",
        "El número 0 té 1 dígit."
    ),
    Problem(
        "BUCLES",
        "Divisible per 3 i per 5",
        "Escriu un programa que donat un nombre enter positiu n, digui quins d’aquests números d’1 fins a n, són divisibles per 3, quins\n" +
                "per 5, i quins pels dos.",
        "Per l’entrada rebreu un nombre enter positiu.",
        "Per la sortida s’ha d’imprimir la llista dels nombres divisibles per 3, per 5 o pels dos.",
        "20",
        "3 divisible per 3\n" +
                "5 divisible per 5\n" +
                "6 divisible per 3\n" +
                "9 divisible per 3\n" +
                "10 divisible per 5\n" +
                "12 divisible per 3\n" +
                "15 divisible per 3 i per 5\n" +
                "18 divisible per 3\n" +
                "20 divisible per 5",
        "16",
        "3 divisible per 3\n" +
                "5 divisible per 5\n" +
                "6 divisible per 3\n" +
                "9 divisible per 3\n" +
                "10 divisible per 5\n" +
                "12 divisible per 3\n" +
                "15 divisible per 3 i per 5"
    ),
    Problem(
        "BUCLES",
        "Extrems",
        "Escriu un programa que vagi rebent per entrada nombres enters, fins que l’usuari introdueixi un 0, que marca la fi de la\n" +
                "seqüència, llavors digui quin és el nombre més alt i el més baix.",
        "Per l’entrada rebreu una sèrie finita de nombres enters acabada en 0.",
        "Per la sortida s’ha d’imprimir el nombre més alt i el més baix que s’han introduït (sense comptar el 0).",
        "2 6 -1 9 -5 15 -230 16 0",
        "16 -230",
        "6 0",
        "6 6"
    ),
    Problem(
        "LLISTES",
        "Calcula la mitjana",
        "Feu un programa que rebi per paràmetre una seqüència no buida d’enters i calculi la mitjana de tots els valors.",
        "L’entrada consisteix en una seqüència no buida d’enters.",
        "Escriviu el resultat de la mitjana de tots els enters amb decimals.",
        "1 7 3 2 4 7 5 8 7",
        "4.888888888888889",
        "-3 10 0 -2 8",
        "2.6"
    ),
    Problem(
        "LLISTES",
        "Calcula la lletra del dni",
        "Fes un programa que, donat un número de dni, et calculi la lletra que li correspon. Intenta tornar-lo a solucionar fent servir un\n" +
                "array, perquè el codi sigui molt més reduït que inicialment.",
        "Per l’entrada rebreu un nombre enter, que representa un número de dni, és a dir, un nombre de 8 xifres exactes.",
        "Per la sortida haureu d’imprimir el número del dni amb la seva lletra corresponent..",
        "65004204",
        "65004204V",
        "65004203",
        "65004203Q"
    ),
    Problem(
        "LLISTES",
        "Mínim i màxim",
        "Feu un programa que rebi per paràmetre una seqüència no buida d’enters i mostri el mínim i el màxim valor dins de la\n" +
                "seqüència.",
        "L’entrada consisteix en una seqüència no buida d’enters.",
        "Per la sortida heu d’imprimir el valor mínim i màxim de la seqüència.",
        "2 8 19 -32 189 -1 -68 190 0 1",
        "-68 190",
        "1 5 6 7 10 19 23 43 198 192 200 0",
        "0 200"
    ),
    Problem(
        "LLISTES",
        "Igual a l'últim",
        "Feu un programa que rebi per paràmetre una seqüència no buida d’enters, i que escrigui quants són iguals a l’últim.",
        "L’entrada consisteix en una seqüència no buida d’enters.",
        "Escriviu el nombre d’elements que són iguals a l’últim, aquest exclòs.",
        "1 7 3 2 4 7 5 8 7",
        "2",
        "-3 10 0 -2 8",
        "0"
    ),
    Problem(
        "LLISTES",
        "Quants sumen...?",
        "Feu un programa que rebi per paràmetre una seqüència no buida d’enters no repetits i demani a l’usuari un nombre, s’haurà de\n" +
                "buscar parelles d’enters que sumin aquell nombre.",
        "L’entrada consisteix en un nombre enter.",
        "Per la sortida heu d’imprimir totes les parelles d’enters que sumen aquell nombre.\n" +
                "Array per paràmetre: [ 25, 5, 2, 10, 33, 15, 40, 1, 20, 4, 11 ]",
        "35",
        "25 10\n" +
                "2 33\n" +
                "10 25\n" +
                "33 2\n" +
                "15 20\n" +
                "20 15",
        "15",
        "5 10\n" +
                "10 5\n" +
                "4 11\n" +
                "11 4"
    )
)